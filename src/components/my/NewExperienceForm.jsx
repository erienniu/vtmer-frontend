/**
 * Created by hao.cheng on 2017/4/15.
 */
import React, { Component } from 'react';
import {Button, Modal, Form, Input, Radio, notification} from 'antd';
import axios from "axios";
import * as config from "../../axios/config";
const FormItem = Form.Item;

const CollectionCreateForm = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form } = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="创建"
                okText="创建"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="标题">
                        {getFieldDecorator('Title', {
                            rules: [{ required: true, message: '请输入标题！' }],
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem label="描述">
                        {getFieldDecorator('Detail', {
                            rules: [{ required: true, message:'请输入详细描述！'}],
                        })(
                            <Input type="textarea" />
                        )}
                    </FormItem>
                    {/*<FormItem className="collection-create-form_last-form-item" style={{marginBottom: 0}}>*/}
                        {/*{getFieldDecorator('modifier', {*/}
                            {/*initialValue: 'public',*/}
                        {/*})(*/}
                            {/*<Radio.Group>*/}
                                {/*<Radio value="public">公开</Radio>*/}
                                {/*<Radio value="private">私有</Radio>*/}
                            {/*</Radio.Group>*/}
                        {/*)}*/}
                    {/*</FormItem>*/}
                </Form>
            </Modal>
        );
    }
);

class ModalForm extends Component {
    state = {
        visible: false,
    };
    showModal = () => {
        this.setState({ visible: true });
    };
    handleCancel = () => {
        this.setState({ visible: false });
    };
    handleCreate = () => {
        const form = this.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            axios.post(config.BASE_BACKEND_URL+"experience/"+this.props.groupid,
                values,
                {headers: {'Content-Type' : 'application/x-www-form-urlencoded;charset=utf-8'}}
            );
            form.resetFields();
            this.setState({ visible: false });
            notification['success']({
                message: '成功创建',
                description: '已提交创建请求，请稍后刷新查看。',
            });
        });
    };
    saveFormRef = (form) => {
        this.form = form;
    };
    render() {
        return (
            <div>
                <Button type="primary" onClick={this.showModal}>新建</Button>
                <CollectionCreateForm
                    ref={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                />
            </div>
        );
    }
}

export default ModalForm;