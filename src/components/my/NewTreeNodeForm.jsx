/**
 * Created by hao.cheng on 2017/4/15.
 */
import React, { Component } from 'react';
import {Button, Modal, Form, Input, Radio, notification, Select} from 'antd';
import axios from "axios";
import * as config from "../../axios/config";
const FormItem = Form.Item;
const Option = Select.Option;

const CollectionCreateForm = Form.create()(
    (props) => {
        const { visible, onCancel, onCreate, form, options } = props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title="创建"
                okText="创建"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <Form.Item
                        label="父级标题"
                    >
                        {getFieldDecorator('Father', {
                            rules: [{ required: true, message: '请选择父级标题' }],
                        })(
                            <Select
                                showsearch
                                placeholder="请选择父级标题"
                            >
                                {options.map((option) => {
                                    return <Option key={option.Id} value={option.Id}>{option.Title}</Option>
                                })}
                            </Select>
                        )}
                    </Form.Item>
                    <FormItem label="标题">
                        {getFieldDecorator('Title', {
                            rules: [{ required: true, message:'请输入标题！'}],
                        })(
                            <Input/>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
);

class ModalForm extends Component {
    state = {
        visible: false,
    };
    showModal = () => {
        this.setState({ visible: true });
    };
    handleCancel = () => {
        this.setState({ visible: false });
    };
    handleCreate = () => {
        const newTreeNode = this.props.onCreate;
        const form = this.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            newTreeNode(values);
            // axios.post(config.BASE_BACKEND_URL+"experience/"+this.props.groupid,
            //     values,
            //     {headers: {'Content-Type' : 'application/x-www-form-urlencoded;charset=utf-8'}}
            // );
            form.resetFields();
            this.setState({ visible: false });
            notification['success']({
                message: '成功创建',
                description: '已提交创建请求，请稍后刷新查看。',
            });
        });
    };
    saveFormRef = (form) => {
        this.form = form;
    };
    readNode = (node,result) => {
        for (let i=0;i<node.length;i++){
            result.push({
                Id: node[i].Id,
                Title: node[i].Title,
            });
            if (node[i].children && Object.prototype.toString.call(node[i].children) == '[object Array]' && node[i].children.length>0)
            {
                this.readNode(node[i].children, result)
            }
        }

    };
    render() {
        let arr = [];
        this.readNode(this.props.options, arr);
        return (
            <div>
                <Button type="primary" onClick={this.showModal}>新建</Button>
                <CollectionCreateForm
                    ref={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    options ={arr}
                />
            </div>
        );
    }
}

export default ModalForm;