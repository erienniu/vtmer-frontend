/**
 * Created by hao.cheng on 2017/4/25.
 */
import React, { Component } from 'react';
import {Row, Col, Card, Tabs, Icon, Radio, Button, Tree, Skeleton} from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
const TabPane = Tabs.TabPane;

class Document extends Component {
    constructor(props) {
        super(props);
        this.newTabIndex = 0;
        const panes = [
            { title: 'Tab 1', content: 'Content of Tab Pane 1', key: '1' },
            { title: 'Tab 2', content: 'Content of Tab Pane 2', key: '2' },
        ];
        this.state = {
            activeKey: panes[0].key,
            panes,
            mode: 'top'
        };
    }

    onChange = (activeKey) => {
        this.setState({ activeKey });
    };
    onEdit = (targetKey, action) => {
        this[action](targetKey);
    };
    add = () => {
        const panes = this.state.panes;
        const activeKey = `newTab${this.newTabIndex++}`;
        panes.push({ title: 'New Tab', content: 'New Tab Pane', key: activeKey });
        this.setState({ panes, activeKey });
    };
    remove = (targetKey) => {
        let activeKey = this.state.activeKey;
        let lastIndex;
        this.state.panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const panes = this.state.panes.filter(pane => pane.key !== targetKey);
        if (lastIndex >= 0 && activeKey === targetKey) {
            activeKey = panes[lastIndex].key;
        }
        this.setState({ panes, activeKey });
    };
    render() {
        return (
            <div className="gutter-example">
                <BreadcrumbCustom first="表格" second="高级表格" />
                <Row>
                    <Col span={4}>
                        <Tree
                            showLine
                            defaultExpandedKeys={['0-0-0']}
                        >
                            <TreeNode title="parent 1" key="0-0">
                                <TreeNode title="parent 1-0" key="0-0-0">
                                    <TreeNode title="leaf" key="0-0-0-0" />
                                    <TreeNode title="leaf" key="0-0-0-1" />
                                    <TreeNode title="leaf" key="0-0-0-2" />
                                </TreeNode>
                                <TreeNode title="parent 1-1" key="0-0-1">
                                    <TreeNode title="leaf" key="0-0-1-0" />
                                </TreeNode>
                                <TreeNode title="parent 1-2" key="0-0-2">
                                    <TreeNode title="leaf" key="0-0-2-0" />
                                    <TreeNode title="leaf" key="0-0-2-1" />
                                </TreeNode>
                            </TreeNode>
                        </Tree>
                    </Col>
                    <Col span={20}>
                            <div className="gutter-box">
                                <Card title="带删除和新增" bordered={false}>
                                    <div style={{ marginBottom: 16 }}>
                                        <Button onClick={this.add}>ADD</Button>
                                    </div>
                                    <Tabs
                                        hideAdd
                                        onChange={this.onChange}
                                        activeKey={this.state.activeKey}
                                        type="editable-card"
                                        onEdit={this.onEdit}
                                    >
                                        {this.state.panes.map(pane => <TabPane tab={pane.title} key={pane.key}>{pane.content}</TabPane>)}
                                    </Tabs>
                                </Card>
                            </div>
                        <Skeleton />
                    </Col>
                </Row>

            </div>
        )
    }
}

export default Document;