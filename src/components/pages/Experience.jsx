/**
 * Created by hao.cheng on 2017/4/16.
 */
import React from 'react';
import { Table, Button, Row, Col, Card, Modal, notification, Icon } from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import * as config from "../../axios/config";
import {get} from "../../axios/tools";
import ModalForm from '../my/NewExperienceForm'
import axios from 'axios';

const columns = [{
    title: '标题',
    dataIndex: 'Title',
    width: 200,
    render: (text) => <a target="_blank" rel="noopener noreferrer">{text}</a>,
}, {
    title:  '描述',
    dataIndex: 'Detail',
    width: 350,
}, {
    title: '作者',
    dataIndex: 'Author',
    width: 60,
}, {
    title: '发布时间',
    dataIndex: 'CreatedAt',
    width: 100,
}
];

class AsynchronousTable extends React.Component {
    constructor(props){
        super(props);
        let arr = this.props.location.pathname.split('/');
        switch (arr[arr.length-1]) {
            case '1':
                this.state.groupname = '设计组';
                this.state.groupid = 1;
                break;
            case '2':
                this.state.groupname = '前端组';
                this.state.groupid = 2;
                break;
            case '3':
                this.state.groupname = '后台组';
                this.state.groupid = 3;
                break;
            case '4':
                this.state.groupname = '运营组';
                this.state.groupid = 4;
                break;
            default:
                break;
        }
    }
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        visible: false,
        data: [],
        groupname: '',
        groupid: 0,
    };
    componentDidMount() {
        this.start();
    }

    start = () => {
        this.setState({ loading: true, data: null });
        get({ url: config.BASE_BACKEND_URL+"experience/"+this.state.groupid }).then((res) => {
            let tmp = [];
            if (Object.prototype.toString.call(res.data) == '[object Array]'){
                for (let i=0; i<res.data.length; i++){
                    tmp.push({
                        key: res.data[i].Id,
                        Detail: res.data[i].Detail,
                        Title: res.data[i].Title,
                        Author: res.data[i].Author,
                        CreatedAt: res.data[i].CreatedAt,
                    })
                }
            }
            this.setState({
                data: tmp,
                loading: false
            });
        });
    };
    showModal = () => {

    };
    newTreeNode = () => {};

    showWarning = () => {
      this.setState({ visible: true});
    };
    delete = () => {
        let reqList = [];
        for (let i=0; i<this.state.selectedRowKeys.length; i++){
            reqList.push(
                axios.delete(config.BASE_BACKEND_URL+"/experience/"+this.state.groupid+'/'+this.state.selectedRowKeys[i])
            )
        }
        axios.all(reqList);
        this.setState({visible:false, selectedRowKeys: []});
        this.start();
        notification['success']({
            message: '成功提交',
            description: '已提交删除请求，请稍后刷新查看。',
            // icon: <Icon type="smile" style={{ color: '#108ee9' }} />,
        });
    };
    cancel = () => {
        this.setState({visible:false})
    };
    onSelectChange = (selectedRowKeys) => {
        this.setState({ selectedRowKeys });
        console.log(selectedRowKeys)
    };
    render() {
        const { loading, selectedRowKeys } = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const hasSelected = selectedRowKeys.length > 0;

        return (
            <div className="gutter-example">

                <Modal title="删除" visible={this.state.visible}
                       onOk={this.delete} onCancel={this.cancel}
                >
                    <p>确认删除？</p>
                </Modal>

                <BreadcrumbCustom first="面试经验" second={ this.state.groupname } />
                <Row gutter={16}>
                    <Col className="gutter-row" md={24}>
                        <div className="gutter-box">
                            <Card title="面试经验" bordered={false}>
                                <div style={{ display: "flex", flexDirection: "row", marginBottom: 8 }}>
                                    <div style={{ marginRight:8 }}>
                                        <Button type="primary" onClick={this.start}
                                                disabled={loading} loading={loading}
                                        >刷新</Button>
                                    </div>
                                    <div style={{ marginRight:8 }}>
                                        <ModalForm groupid={this.state.groupid} />
                                    </div>
                                    <div style={{ marginRight:8 }}>
                                        <Button type="danger" onClick={this.showWarning}
                                                style={{display: hasSelected?'inline':'none'}}
                                        >{ `删除 ${selectedRowKeys.length} 条` }</Button>
                                    </div>
                                </div>

                                <Table rowSelection={rowSelection}  columns={columns} dataSource={this.state.data} />
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AsynchronousTable;