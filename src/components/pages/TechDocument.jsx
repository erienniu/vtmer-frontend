/**
 * Created by hao.cheng on 2017/4/25.
 */
import React, { Component } from 'react';
import {Row, Col, Card, Tabs, Icon, Radio, Button, Tree, Skeleton, Spin, notification} from 'antd';
import BreadcrumbCustom from '../BreadcrumbCustom';
import axios from "axios";
import * as config from "../../axios/config"
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import draftToHtml from 'draftjs-to-html';
import { convertFromRaw } from 'draft-js';
import ModalForm from "../my/NewTreeNodeForm";

const TabPane = Tabs.TabPane;
const { TreeNode } = Tree;

class TechDocument extends Component {
    constructor(props) {
        super(props);
        this.newTabIndex = 0;
        const panes = [
            { title: '维生数工作室', content:
                    {"blocks":[
                        {"key":"9dmqn","text":"欢迎加入维生数工作室！","type":"header-three","depth":0,"inlineStyleRanges":[{"offset":0,"length":11,"style":"color-rgba(0,0,0,0.85)"},{"offset":0,"length":11,"style":"bgcolor-rgb(255,255,255)"},{"offset":0,"length":11,"style":"fontfamily-sans-serif"},{"offset":0,"length":11,"style":"BOLD"}],"entityRanges":[],"data":{"text-align":"start"}},
                            {"key":"a2gcb","text":"维生数工作室 今年十岁啦！","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":6,"style":"color-rgb(0,0,0)"},{"offset":7,"length":6,"style":"color-rgb(0,0,0)"},{"offset":0,"length":6,"style":"bgcolor-rgb(255,255,255)"},{"offset":7,"length":6,"style":"bgcolor-rgb(255,255,255)"},{"offset":0,"length":6,"style":"fontsize-14"},{"offset":7,"length":6,"style":"fontsize-14"},{"offset":0,"length":6,"style":"fontfamily-sans-serif"},{"offset":7,"length":6,"style":"fontfamily-sans-serif"},{"offset":0,"length":6,"style":"color-rgb(49,54,83)"},{"offset":0,"length":6,"style":"bgcolor-transparent"},{"offset":0,"length":6,"style":"BOLD"}],"entityRanges":[{"offset":0,"length":6,"key":0}],"data":{"text-align":"start"}},
                            {"key":"ehf6f","text":"很高兴优秀的你加入了我们，希望未来的几年中，你能更加优秀。","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":29,"style":"color-rgb(0,0,0)"},{"offset":0,"length":29,"style":"bgcolor-rgb(255,255,255)"},{"offset":0,"length":29,"style":"fontsize-14"},{"offset":0,"length":29,"style":"fontfamily-sans-serif"}],"entityRanges":[],"data":{"text-align":"start"}},
                            {"key":"7b4m0","text":"共勉之。 ","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":4,"style":"color-rgb(0,0,0)"},{"offset":0,"length":4,"style":"bgcolor-rgb(255,255,255)"},{"offset":0,"length":4,"style":"fontsize-14"},{"offset":0,"length":4,"style":"fontfamily-sans-serif"}],"entityRanges":[],"data":{"text-align":"start"}}],"entityMap":{"0":{"type":"LINK","mutability":"MUTABLE","data":{"url":"https://vtmer.obstacle.cn/","title":"<strong style=\"box-sizing: border-box; font-weight: bolder;\">维生数工作室</strong>","targetOption":"_self","_map":{"type":"LINK","mutability":"MUTABLE","data":{"url":"https://vtmer.obstacle.cn/","title":"<strong style=\"box-sizing: border-box; font-weight: bolder;\">维生数工作室</strong>","targetOption":"_self"}}}}}},
                key: '0' },
        ];
        this.state = {
            activeKey: panes[0].key,
            panes,
            mode: 'top',
            father: 0,
            treeData: [],
            loading: false,
        };
    }
    state = {
        editorContent: {},
        contentState: {},
    };

    componentDidMount() {
        this.start();
    }

    onContentStateChange = (contentState) => {
        this.setState({
            contentState,
        });
    };

    start = () => {
        let that = this;
        axios.get(config.BASE_BACKEND_URL+"document/list/"+0)
            .then(function (res) {
                if (res.data.status == 200){
                    that.setState({
                        treeData: [...res.data.data],
                    });
                }
            })
    };
    onChange = (activeKey) => {
        this.setState({ activeKey });
    };
        onEdit = (targetKey, action) => {
        this[action](targetKey);
    };
    select = (res) => {
        if (res[0]) this.add(res[0]);
    };
    add = (id) => {
        this.setState({loading: true});
        let title = "";
        let content = "";
        const panes = this.state.panes;
        const activeKey = `newTab${this.newTabIndex++}`;
        let that = this;
        axios.get(config.BASE_BACKEND_URL+"document/one/"+id)
            .then(function (res) {
                title = res.data.data.Title;
                try {
                    content = JSON.parse(res.data.data.Detail);
                    that.onContentStateChange(content);
                }catch (e) {
                    notification['error']({
                        message: '解析失败',
                        description: '请重新更新文章',
                    })
                }
                panes.push({ title: title, content: content, key: activeKey });
                that.setState({ panes, activeKey, loading:false });
            });
    };
    remove = (targetKey) => {
        let activeKey = this.state.activeKey;
        let lastIndex;
        this.state.panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
            }
        });
        const panes = this.state.panes.filter(pane => pane.key !== targetKey);
        if (lastIndex >= 0 && activeKey === targetKey) {
            activeKey = panes[lastIndex].key;
        }
        this.setState({ panes, activeKey });
    };
    onLoadData = treeNode => new Promise((resolve) => {
        this.setState({father: treeNode.props.Id});
        if (treeNode.props.children) {
            resolve();
            return;
        }
        setTimeout(() => {
            let that = this;
            axios.get(config.BASE_BACKEND_URL+"document/list/"+this.state.father)
                .then(function (res) {
                    if (res.data.status == 200){
                        treeNode.props.dataRef.children = res.data.data;
                        that.setState({
                            treeData: [...that.state.treeData],
                        });
                    }
                });
            resolve();
        }, 500);
    });
    renderTreeNodes = data => data.map((item) => {
        if (item.children) {
            return (
                <TreeNode title={item.Title} key={item.Id} dataRef={item}>
                    {this.renderTreeNodes(item.children)}
                </TreeNode>
            );
        }
        return <TreeNode title={item.Title} key={item.Id} {...item} dataRef={item} />;
    });
    newTreeNode = (values) => {
        axios.post(config.BASE_BACKEND_URL+"document/one/",
            values,
            {headers: {'Content-Type' : 'application/x-www-form-urlencoded;charset=utf-8'}}
        );
    };
    render() {
        return (
            <div className="gutter-example">
                <BreadcrumbCustom first="技术文档"/>
                <Row>
                    <Col span={4} style={{marginRight:"8px"}}>
                        <Card title="目录" bordered={false}>
                            <div style={{ display: "flex", flexDirection: "row", marginBottom: 8 }}>
                                <div style={{ marginRight:8 }}>
                                    <ModalForm onCreate={this.newTreeNode} options={this.state.treeData}/>
                                </div>
                            </div>


                            <div>
                            <Tree
                                showLine
                                defaultExpandedKeys={['0']}
                                onSelect={this.select}
                                loadData={this.onLoadData}
                            >
                                    {this.renderTreeNodes(this.state.treeData)}
                                </Tree>
                        </div>
                        </Card>
                    </Col>
                    <Col span={19}>
                            <Card title="技术文档" bordered={false}>
                                <Tabs
                                    hideAdd
                                    onChange={this.onChange}
                                    activeKey={this.state.activeKey}
                                    type="editable-card"
                                    onEdit={this.onEdit}
                                >
                                    {this.state.panes.map(pane => <TabPane tab={pane.title} key={pane.key}>
                                        <Spin size="large" spinning={this.state.loading} />
                                        <div dangerouslySetInnerHTML={{__html: this.state.loading? "":draftToHtml(pane.content)}} />
                                    </TabPane>)}
                                </Tabs>
                            </Card>
                        <Card bordered={false}>
                            <Editor
                                contentState={this.state.contentState}
                                toolbarClassName="home-toolbar"
                                wrapperClassName="home-wrapper"
                                editorClassName="home-editor"
                                toolbar={{
                                    history: { inDropdown: true },
                                    inline: { inDropdown: false },
                                    list: { inDropdown: true },
                                    textAlign: { inDropdown: true },
                                    // image: { uploadCallback: this.imageUploadCallBack },
                                }}
                                onContentStateChange={this.onContentStateChange}
                                spellCheck
                                localization={{ locale: 'zh', translations: {'generic.add': 'Test-Add'} }}
                            /><style>{`
                                    .home-editor {
                                        min-height: 400px;
                                    }
                                `}</style>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default TechDocument;