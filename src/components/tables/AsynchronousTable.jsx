/**
 * Created by hao.cheng on 2017/4/16.
 */
import React from 'react';
import { Table, Button, Row, Col, Card } from 'antd';
import { getBbcNews } from '../../axios';
import BreadcrumbCustom from '../BreadcrumbCustom';
import * as config from "../../axios/config";
import {get} from "../../axios/tools";

const columns = [{
    title: '标题',
    dataIndex: 'Title',
    width: 200,
    render: (text, record) => <a href={record.url} target="_blank" rel="noopener noreferrer">{text}</a>
}, {
    title:  '描述',
    dataIndex: 'Detail',
    width: 500
}, {
    title: '作者',
    dataIndex: 'author',
    width: 60
}, {
    title: '发布时间',
    dataIndex: 'publishedAt',
    width: 60
}, {
    title: '编辑',
    width: 10,
    render: () => <Button>编辑</Button>
}, {
    title: '删除',
    width: 10,
    render: () => <Button type="danger">删除</Button>
}
];

class AsynchronousTable extends React.Component {
    constructor(props){
        super(props);
        let arr = this.props.location.pathname.split('/');
        switch (arr[arr.length-1]) {
            case '1':
                this.state.groupname = '设计组';
                this.state.groupid = 1;
                break;
            case '2':
                this.state.groupname = '前端组';
                this.state.groupid = 2;
                break;
            case '3':
                this.state.groupname = '后台组';
                this.state.groupid = 3;
                break;
            case '4':
                this.state.groupname = '运营组';
                this.state.groupid = 4;
                break;
            default:
                break;
        }
    }
    state = {
        selectedRowKeys: [], // Check here to configure the default column
        loading: false,
        data: [],
        groupname: '',
        groupid: 0,
    };
    componentDidMount() {
        this.start();
    }
    start = () => {
        this.setState({ loading: true });
        get({ url: config.BASE_BACKEND_URL+"/experience/"+this.state.groupid }).then((res) => {
            console.log(res.data);
            this.setState({
                data: res.data,
                loading: false
            });
        });
    };
    render() {
        const { loading } = this.state;

        return (
            <div className="gutter-example">
                <BreadcrumbCustom first="面试经验" second={ this.state.groupname } />
                <Row gutter={16}>
                    <Col className="gutter-row" md={24}>
                        <div className="gutter-box">
                            <Card title="面试经验" bordered={false}>
                                <div style={{ marginBottom: 16 }}>
                                    <Button type="primary" onClick={this.start}
                                            disabled={loading} loading={loading}
                                    >刷新</Button>
                                </div>
                                <Table columns={columns} dataSource={this.state.data}/>
                            </Card>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default AsynchronousTable;